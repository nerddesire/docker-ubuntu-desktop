
FROM ubuntu:16.04

ARG PASSWORD="password"

ENV DEBIAN_FRONTEND noninteractive
ENV USER root
ENV XKL_XMODMAP_DISABLE=1

# Install required packages
RUN apt-get update
RUN apt-get install -y --no-install-recommends ubuntu-desktop
RUN apt-get install -y gnome-panel 
RUN apt-get install -y gnome-settings-daemon 
RUN apt-get install -y metacity 
RUN apt-get install -y nautilus 
RUN apt-get install -y gnome-terminal
RUN apt-get install -y tightvncserver

# Configure vnc folder
RUN mkdir /root/.vnc
ADD xstartup /root/.vnc/xstartup
RUN chmod +x /root/.vnc/xstartup

# Configure password
RUN echo $PASSWORD | vncpasswd -f > /root/.vnc/passwd
RUN chmod 600 /root/.vnc/passwd

EXPOSE 5901

ENTRYPOINT /usr/bin/vncserver :1 -geometry 1280x800 -depth 24 && tail -f /root/.vnc/*:1.log